function buildChart(chartData, minDate, maxDate)
{
    let ctx = document.getElementById('mainchart').getContext('2d');

    console.log(chartData);

    let chart = new Chart(ctx, {
        type: 'bar',
    
        data: {
            datasets: [{
                label: '9500MW Safety Line',
                type: 'line',
                borderColor: 'rgb(58, 190, 255)',
                backgroundColor: 'rgba(195, 235, 255, 0.7)',
                data: [
                    { x: minDate, y: 9500 },
                    { x: maxDate, y: 9500 },
                ]
            },
            {
                label: 'Unplanned Breakdown / UCLF (MW)',
                backgroundColor: 'rgb(255, 91, 91)',
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                data: chartData.unplanned
            },
                {
                    label: 'Planned Maintenance',
                    backgroundColor: 'rgb(255,200,24)',
                    barPercentage: 0.5,
                    barThickness: 6,
                    maxBarThickness: 8,
                    minBarLength: 2,
                    data: chartData.maintenance
                }]
        },
    
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                    type: "time",
                    stacked: true,
                    time: {
                        unit: 'day'
                    },
                    offset: true
                }],
                yAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Megawatts'
                    },
                    ticks: {
                        min: 0
                    }
                }]
            }
        }
    });
    
}

function fetchData()
{
    let txtFile = new XMLHttpRequest();
    let url = "/data.csv?_cb=" + Math.random();
    txtFile.open("GET", url, true);

    txtFile.onreadystatechange = function()
    {
        allText = txtFile.responseText;
        allTextLines = allText.split(/\r\n|\n/);

        let unplanned = [];
        let maintenance = [];
        let minDate = null;
        let maxDate = null;

        for(let i = 0; i < allTextLines.length; i++)
        {
            let element = allTextLines[i].split(',');
            if (element.length != 3) continue;

            unplanned.push({
                x: element[0],
                y: parseInt(element[1])
            });

            maintenance.push({
                x: element[0],
                y: parseInt(element[2])
            });

            if (minDate == null) minDate = element[0];
            maxDate = element[0];

        }

        $("#lastupdated").html(maxDate);

        let data = {
            unplanned: unplanned,
            maintenance: maintenance
        };

        buildChart(data, minDate, maxDate);

    };

    txtFile.send();

}

// Away we go!
fetchData();
